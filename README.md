## Feegow-Challenge

Proposta:
A clínica Exemplo precisa exibir a listagem de seus médicos separados por especialidade em seu site para que seus pacientes tenham acesso.

# Pre-requisitos
``` PHP 8 or above ```<br>
``` MySql ```

# Instalação
* Database <br>
    ``` Diretorio database: Abrir modelo MySql Workbench (db.mwb) ou importar o SQL (schema.sql) ``` <br>
    ``` Alterar DB_USERNAME e DB_PASSWORD no .env ```
    [Link db.mwb](storage/db.mwb)
    [Link schema.mwb](storage/schema.sql)

* Projeto <br>
    ``` composer i ```

# Start
``` iniciar servidor MySql ```<br>
``` php artisan serve ```
