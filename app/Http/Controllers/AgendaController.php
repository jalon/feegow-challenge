<?php
namespace App\Http\Controllers;

use App\Services\AgendaService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AgendaController extends Controller {

    /**
     * @var AgendaService $service
    */
    protected $service;

    public function __construct(AgendaService $service)
    {
        $this->service = $service;
    }

    public function store(Request $request): JsonResponse
    {
        return $this->service->cadastrar($request);
    }
}
