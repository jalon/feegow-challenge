<?php
namespace App\Http\Controllers;

use App\Services\PatientService;
use Illuminate\Http\JsonResponse;

class PatientController extends Controller {

    /**
     * @var PatientService $service
    */
    protected $service;

    public function __construct(PatientService $service)
    {
        $this->service = $service;
    }

    public function list(): JsonResponse
    {
        return $this->service->listar();
    }
}
