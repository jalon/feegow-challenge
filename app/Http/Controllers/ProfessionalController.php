<?php
namespace App\Http\Controllers;

use App\Services\ProfessionalService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProfessionalController extends Controller {

    /**
     * @var ProfessionalService $service
    */
    protected $service;

    public function __construct(ProfessionalService $service)
    {
        $this->service = $service;
    }

    public function list(Request $request): JsonResponse
    {
        return $this->service->listar($request);
    }
}
