<?php
namespace App\Http\Controllers;

use App\Services\SpecialtiesService;
use Illuminate\Http\JsonResponse;

class SpecialtiesController extends Controller {

    /**
     * @var SpecialtiesService $service
    */
    protected $service;

    public function __construct(SpecialtiesService $service)
    {
        $this->service = $service;
    }

    public function list(): JsonResponse
    {
        return $this->service->listar();
    }
}
