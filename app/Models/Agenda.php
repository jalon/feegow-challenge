<?php

namespace App\Models;

class Agenda extends BaseModel
{
    protected $table = 'agenda';
    protected $primaryKey = 'codigo';

    protected $fillable = [
        'codigo',
        'specialty_id',
        'professional_id',
        'name',
        'cpf',
        'source_id',
        'birthdate',
        'date_time'
    ];
}
