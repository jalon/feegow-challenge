<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public $timestamps = false;
    const CREATED_AT = 'DATA_INCLUSAO';
    const UPDATED_AT = 'DATA_ULT_ALTERACAO';
}
