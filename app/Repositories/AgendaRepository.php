<?php

namespace App\Repositories;

use App\Models\Agenda;
use App\Repositories\BaseRepository;


class AgendaRepository extends BaseRepository
{
    protected object $model;

    public function __construct()
    {
        $this->model = app(Agenda::class);
    }
}
