<?php

namespace App\Services;

use App\Repositories\AgendaRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AgendaService
{
    protected object $repository;

    public function __construct(AgendaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function cadastrar(Request $request): JsonResponse
    {
        $request->merge([
            'birthdate' => date("Y-m-d", strtotime(str_replace('/', '-', $request->birthdate))),
            'date_time' => date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $request->date_time)))
        ]);

        $model = $this->repository->cadastrar($request);

        if (is_object($model)) {
            return response()->json([
                'data' => true,
                'message' => 'Consulta agendada com sucesso.',
                'notification' => null,
            ], Response::HTTP_CREATED);
        }

        return response()->json([
            'data' => null,
            'notification' => null,
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
