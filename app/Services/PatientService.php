<?php

namespace App\Services;

use App\Repositories\PatientRepository;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class PatientService
{
    protected object $repository;

    public function __construct(PatientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listar(): JsonResponse
    {
        $retorno = $this->repository->api("/patient/list-sources");

        if(is_array($retorno) && $retorno['success'] == true) {
            return response()->json($retorno['content'], Response::HTTP_OK);
        }
    }
}
