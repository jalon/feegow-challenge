<?php

namespace App\Services;

use App\Repositories\ProfessionalRepository;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProfessionalService
{
    protected object $repository;

    public function __construct(ProfessionalRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listar(Request $request): JsonResponse
    {
        $retorno = $this->repository->api("/professional/list", ['ativo' => 1, 'especialidade_id' => $request->especialidade_id]);

        if(is_array($retorno) && $retorno['success'] == true) {
            return response()->json($retorno['content'], Response::HTTP_OK);
        }
    }
}
