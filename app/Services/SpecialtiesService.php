<?php

namespace App\Services;

use App\Repositories\SpecialtiesRepository;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;

class SpecialtiesService
{
    protected object $repository;

    public function __construct(SpecialtiesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function listar(): JsonResponse
    {
        $retorno = $this->repository->api("/specialties/list");

        if($retorno['success'] == true) {
            return response()->json($retorno['content'], Response::HTTP_OK);
        }
    }
}
