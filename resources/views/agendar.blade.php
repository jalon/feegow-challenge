@extends('main')

@section('conteudo')
<div class="wrapper wrapper--w790">
    <div class="card card-5">
        <div class="card-heading">
            <h2 class="title">Consulta</h2>
        </div>
        <div class="card-body">
            <form id="formAgendarID">
                <input type="hidden" value="{{ request()->specialty_id }}" name="specialty_id">
                <input type="hidden" value="{{ request()->professional_id }}" name="professional_id">
                <div class="section-title">
                    <h2><a href="#" onclick="history.back()"><i class="fa fa-arrow-left"></i></a> Preencha seus dados</h2>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group-desc">
                                <input class="input--style-5" type="text" id="nomeID" name="name" placeholder="Nome completo" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group-desc">
                                <input class="input--style-5" type="text" id="cpfID" name="cpf" placeholder="CPF" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group-desc">
                                <input type="text" class="input--style-5" id="nascimentoID" name="birthdate" placeholder="Nascimento" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group-desc">
                                <input type="text" class="input--style-5" id="dataAgendaID" name="date_time" placeholder="Data agendamento" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-6">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select id="comoConheceuID" name="source_id" required>
                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div>
                    <button class="btn btn--radius-2 btn--green" id="agendarID">Agendar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@section('javascripts')
<script>
    $(function() {
        $('#nascimentoID').datetimepicker({
            "allowInputToggle": true,
            "showClose": true,
            "showClear": true,
            "showTodayButton": true,
            "format": "DD/MM/YYYY",
        });

        $('#dataAgendaID').datetimepicker({
            "allowInputToggle": true,
            "showClose": true,
            "showClear": true,
            "showTodayButton": true,
            "format": "DD/MM/YYYY hh:mm:ss A",
        });

        $('#comoConheceuID').select2({
            placeholder: 'Como conheceu?',
            allowClear: true,
            language: 'pt-BR',
            ajax: {
                url: '{{ route("patient/list") }}',
                dataType: 'json',
                data: function(params) {
                    return {}
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(obj) {
                            return {
                                id: obj.origem_id,
                                text: obj.nome_origem
                            };
                        })
                    };
                },
                cache: true
            }
        });

        $("#agendarID").on("click", function(evt) {
            evt.stopImmediatePropagation();

            if(!$('#formAgendarID').valid()) {
                toastr.error('Dados formulário inválidos!');
                return false;
            }

            $.ajax({
                url: '{{ route("agendar") }}',
                type: "POST",
                data: $("#formAgendarID").serialize()
            }).done(function(res) {
                toastr.success('Consulta agendada com sucesso!');
            });

            return false;
        });
    });
</script>
@endsection
@endsection
