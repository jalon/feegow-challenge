@extends('main')

@section('conteudo')
<div class="wrapper wrapper--w790">
    <div class="card card-5">
        <div class="card-heading">
            <h2 class="title">Consulta</h2>
        </div>
        <div class="card-body">
            <form id="formConsulta">
                <div class="form-row">
                    <div class="name">Consulta de</div>
                    <div class="value">
                        <div class="input-group">
                            <div class="col-md-12">
                                <div class="rs-select2 js-select-simple select--no-search">
                                    <select id="especialidadeID" name="especialidade">
                                    </select>
                                    <div class="select-dropdown"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <section id="doctors" class="doctors" style="display: none;">
                    <div id="doctors_total"></div>
                    <div id="doctors_content" class="container"></div>
                </section>
            </form>
        </div>
    </div>
</div>
@section('javascripts')
<script>
    $(function() {
        $('#especialidadeID').select2({
            placeholder: 'Selecione a especialidade',
            allowClear: true,
            language: 'pt-BR',
            ajax: {
                url: '{{ route("specialties/list") }}',
                dataType: 'json',
                data: function(params) {
                    return {}
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(obj) {
                            return {
                                id: obj.especialidade_id,
                                text: obj.nome
                            };
                        })
                    };
                },
                cache: true
            }
        }).on('select2:select', function(evt) {
            $("#doctors_content").html("");
            $("#doctors_total").html("");

            $.ajax({
                url: '{{ route("professional/list") }}',
                type: "GET",
                data: { especialidade_id: evt.params.data.id }
            }).done(function(res) {
                $("#doctors").show();
                $("#doctors_total").html("<span>" + res.length + " " + evt.params.data.text + " encontrados</span>");

                for (let i=0; i < res.length; i++) {
                    createContent(i, res[i].foto, res[i].nome, res[i].documento_conselho, evt.params.data.id, res[i].profissional_id);
                }
            });
        }).on('select2:unselect', function (evt) {
            $("#doctors_content").html("");
            $("#doctors_total").html("");

            $("#doctors").hide();
        });

        function createContent(i, foto, nome, crm, specialty_id, profissional_id) {
            let $content = $("<div>", {class: "col-md-6"}).append(
                        $("<div>", {class: "member d-flex align-items-start"}).append(
                            $("<div>", {class: "pic"}).append(
                                $("<img>", {class: "img-fluid", src: foto ?? "img/avatar.svg"})
                            ),
                            $("<div>", {class: "member-info"}).append(
                                $("<h4>").text(nome),
                                $("<span>").text('CRM ' + (crm ? crm : 'N/D')),
                                $("<div>", {class: "social"}).append(
                                    $("<a>", {class: "btn btn--radius-2 btn--green", href: "/agendar/" + specialty_id + "/" + profissional_id}).text("Agendar")
                                )
                            )
                        )
                    );

            if (i == 0 || i%2 == 0) {
                $("#doctors_content").append(
                    $("<div>", {id: "doctor_" + i, class: "row"}).append($content)
                );
            } else {
                $("#doctors_content div#doctor_" + (i-1)).append($content);
            }
        }
    });
</script>
@endsection
@endsection
