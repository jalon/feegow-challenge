<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('specialties/list', ['as' => 'specialties/list', 'uses' => 'SpecialtiesController@list']);
Route::get('professional/list', ['as' => 'professional/list', 'uses' => 'ProfessionalController@list']);
Route::get('patient/list', ['as' => 'patient/list', 'uses' => 'PatientController@list']);
Route::post('agendar', ['as' => 'agendar', 'uses' => 'AgendaController@store']);
