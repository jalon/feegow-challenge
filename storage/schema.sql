-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema feegow
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `feegow` ;

-- -----------------------------------------------------
-- Schema feegow
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `feegow` DEFAULT CHARACTER SET utf8 ;
USE `feegow` ;

-- -----------------------------------------------------
-- Table `AGENDA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `AGENDA` (
  `CODIGO` INT NOT NULL AUTO_INCREMENT,
  `SPECIALTY_ID` INT NOT NULL,
  `PROFESSIONAL_ID` INT NOT NULL,
  `NAME` VARCHAR(200) NOT NULL,
  `CPF` VARCHAR(15) NOT NULL,
  `SOURCE_ID` INT NOT NULL,
  `BIRTHDATE` DATE NOT NULL,
  `DATE_TIME` DATETIME NOT NULL,
  PRIMARY KEY (`CODIGO`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
